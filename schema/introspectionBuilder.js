/**
 * Run in order to populate
 * @type {fetch}
 */
const fetch = require('node-fetch');

const fs = require('fs');

// ES6 imports not working here. Hardcoding...
// import {endpoint} from '../config'
const endpoint = `http://localhost:4444`;
const filepath = './schema/fragmentTypes.json';


fetch(`${endpoint}`, {
  method: 'POST',
  headers: { 'Content-Type': 'application/json' },
  body: JSON.stringify({
    variables: {},
    query: `
      {
        __schema {
          types {
            kind
            name
            possibleTypes {
              name
            }
          }
        }
      }
    `,
  }),
})
.then(result => result.json())
.then(result => {
  // here we're filtering out any type information unrelated to unions or interfaces
  const filteredData = result.data.__schema.types.filter(
    type => type.possibleTypes !== null,
  );
  result.data.__schema.types = filteredData;
  fs.writeFile(filepath, JSON.stringify(result.data), err => {
    if (err) {
      console.error('Error writing fragmentTypes file', err);
    } else {
      console.log('Fragment types successfully extracted!');
    }
  });
});
