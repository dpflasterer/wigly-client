import {mount} from 'enzyme';
import toJSON from 'enzyme-to-json';
import {MockedProvider} from 'react-apollo/test-utils';
import wait from 'waait';
import Search, {ResultList, ResultItem, AUTOCOMPLETE_ITEMS} from '../components/Search';
import {LoadingInput} from "../components/common/Inputs";
import {SearchEmpty, SearchResult} from "../__mocks__/Search";

const type = (wrapper, name, value) => {
  wrapper.find(`input[name="${name}"]`).simulate('change', {target: {name, value}});
};

describe('<Search />', () => {
  const mocks = [
    {
      request: {query: AUTOCOMPLETE_ITEMS, variables: {search: 'baobab'}},
      result: {data: SearchResult}
    },
    {
      request: {query: AUTOCOMPLETE_ITEMS, variables: {search: 'asd'}},
      result: {data: SearchEmpty}
    },
  ];

  it('renders', async () => {
    const wrapper = mount(
      <MockedProvider mocks={mocks}>
        <Search />
      </MockedProvider>
    );
    await wait();
    wrapper.update();

    const input = wrapper.find(LoadingInput);
    expect(toJSON(input)).toMatchSnapshot();

    // verify list is populated
    const list = wrapper.find(ResultList);
    expect(toJSON(list)).toMatchSnapshot();

  });

  it('renders results properly', async () => {
    const wrapper = mount(
      <MockedProvider mocks={mocks}>
        <Search delay={0}/>
      </MockedProvider>
    );
    await wait();
    wrapper.update();

    type(wrapper, 'search', 'baobab');

    const populatedInput = wrapper.find(LoadingInput);
    expect(toJSON(populatedInput)).toMatchSnapshot();

    // wait and update once for debounce
    await wait(0);
    wrapper.update();

    // again for ajax request
    await wait(0);
    wrapper.update();

    // verify list is populated
    const list = wrapper.find(ResultList);
    expect(toJSON(list)).toMatchSnapshot();

    const listItems = list.find(ResultItem);
    expect(listItems).toHaveLength(2);

  });

  it('renders no results properly', async () => {
    const wrapper = mount(
      <MockedProvider mocks={mocks}>
        <Search delay={0}/>
      </MockedProvider>
    );
    await wait();
    wrapper.update();

    type(wrapper, 'search', 'asd');

    // wait and update once for debounce
    await wait(0);
    wrapper.update();

    // again for ajax request
    await wait(0);
    wrapper.update();

    // verify list is not populated
    const list = wrapper.find(ResultList);
    expect(toJSON(list)).toMatchSnapshot();

  });

  it('clears results properly', async () => {
    const wrapper = mount(
      <MockedProvider mocks={mocks}>
        <Search delay={0}/>
      </MockedProvider>
    );
    await wait();
    wrapper.update();

    type(wrapper, 'search', 'baobab');

    // wait and update once for debounce
    await wait(0);
    wrapper.update();

    // again for ajax request
    await wait(0);
    wrapper.update();

    // list is now populated, clear it
    type(wrapper, 'search', '');

    // wait and update once for debounce
    await wait(0);
    wrapper.update();

    // again for ajax request
    await wait(0);
    wrapper.update();

    // verify list is populated
    const emptyList = wrapper.find(ResultList);
    expect(toJSON(emptyList)).toMatchSnapshot();

  });

});
