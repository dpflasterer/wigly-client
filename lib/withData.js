import withApollo from 'next-with-apollo';
import ApolloClient from 'apollo-boost';
import { IntrospectionFragmentMatcher, InMemoryCache } from 'apollo-cache-inmemory';
import introspectionQueryResultData from '../schema/fragmentTypes.json';
import { endpoint } from '../config';

// helps with using fragments and unions
// see section on IntrospectionFragmentMatcher: https://www.apollographql.com/docs/react/advanced/fragments.html
const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData
});
const cache = new InMemoryCache({ fragmentMatcher });

function createClient({ headers }) {
  return new ApolloClient({
    cache,
    uri: process.env.NODE_ENV === 'development' ? endpoint : endpoint,
    request: operation => {
      operation.setContext({
        fetchOptions: {
          credentials: 'include', // todo: figure out why this breaks things
        },
        headers,
      });
    },
  });
}

export default withApollo(createClient);
