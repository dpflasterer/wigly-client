import CreateItem from '../components/CreateItem'
import PleaseSignIn from '../components/auth/PleaseSignIn';

const Create = props => (
  <div>
    <PleaseSignIn>
      <CreateItem />
    </PleaseSignIn>
  </div>
);
export default Create;