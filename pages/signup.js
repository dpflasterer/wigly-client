import Signup from '../components/auth/Signup';
import Signin from '../components/auth/Signin';
import RequestReset from '../components/auth/RequestReset';
import styled from 'styled-components';

const SignUpPage = props => (
  <Columns>
    <Signup />
    <Signin />
    <RequestReset />
  </Columns>
);
export default SignUpPage;

const Columns = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
  grid-gap: 20px;
`;