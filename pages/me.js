import {Me} from '../components/auth/UserProfile';
import PleaseSignIn from '../components/auth/PleaseSignIn';

const MePage = props => (
  <div>
    <PleaseSignIn>
      <Me />
    </PleaseSignIn>
  </div>
);
export default MePage;