import Permissions from '../components/auth/Permissions';
import PleaseSignIn from '../components/auth/PleaseSignIn';

const PermissionsPage = props => (
  <div>
    <PleaseSignIn>
      <Permissions />
    </PleaseSignIn>
  </div>
);
export default PermissionsPage;