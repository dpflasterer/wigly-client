import {AdminUser} from '../components/auth/UserProfile';
import PleaseSignIn from '../components/auth/PleaseSignIn';

const UserPage = props => (
  <div>
    <PleaseSignIn>
      <AdminUser query={props.query}/>
    </PleaseSignIn>
  </div>
);
export default UserPage;