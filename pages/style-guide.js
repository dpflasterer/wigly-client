import Link from 'next/link'
import {Button} from "../components/common/Buttons";
import Form from "../components/common/Form";

export default () => (
  <div>
    <h1>Wigly - h1</h1>
    <h2>Style Guide - h2</h2>
    <h3>h3</h3>
    <h4>h4</h4>
    <h5>h5</h5>

    <ul>
      <li>1</li>
      <li>2</li>
      <li>3</li>
    </ul>

    <ol>
      <li>1</li>
      <li>2</li>
      <li>3</li>
    </ol>

    <Button>Button</Button>

    <Link href="/style-guide" prefetch>
      <a>Link</a>
    </Link>

    <blockquote>blockquote</blockquote>
    <code>code</code>

    <p>Plain, <strong>Strong</strong>, <em>em</em>. Vestibulum ac arcu mauris. Maecenas vel risus blandit, accumsan magna sed, finibus odio. Maecenas turpis ipsum, mollis at nunc malesuada, sodales commodo sem. Donec nec mattis ligula. In ut aliquet lorem. Proin placerat vel leo sit amet auctor. Cras vitae mattis arcu. Maecenas vel vehicula ligula, id tempor ex. Vivamus congue efficitur mauris. Duis pellentesque auctor imperdiet.</p>
    <p>Fusce semper auctor arcu vel lacinia. Fusce ac <Link href="/style-guide"><a>tellus vel odio maximus</a></Link> rutrum. Nullam consectetur ipsum et diam scelerisque, nec mattis turpis luctus. Sed semper dui finibus lectus sagittis, consectetur pharetra leo dapibus. Fusce maximus scelerisque accumsan. Nulla tristique, tortor quis finibus pharetra, dui tortor vestibulum quam, a volutpat ante nisi eu velit. Sed dictum porttitor ante vel iaculis. Phasellus imperdiet viverra tortor sed ultricies. Pellentesque dignissim non tellus congue cursus.</p>
    <p>Mauris tincidunt, purus in hendrerit egestas, tellus dolor rutrum urna, sed pretium nunc tellus vitae nunc. Fusce vestibulum porttitor ipsum, eget suscipit erat sollicitudin a. Mauris in diam tortor. Fusce tempor posuere laoreet. Sed id blandit elit, id semper sem. Morbi pulvinar sodales ipsum. Cras porttitor felis et ex ultricies volutpat. Duis justo mauris, iaculis et ultrices sed, laoreet nec felis. Duis a blandit est, sit amet pretium nulla. Integer orci nibh, varius vitae malesuada ac, placerat blandit augue. Donec id tempor turpis. Vivamus ultrices, tortor sit amet mattis congue, metus eros consectetur augue, vitae ultricies purus felis quis enim.</p>

    <Form>
      <fieldset>
        <label htmlFor="text">
          Text
          <input type="text" name="text"/>
        </label>

        <label htmlFor="number">
          Number
          <input type="number" name="number"/>
        </label>

        <label htmlFor="email">
          Email
          <input type="email" name="email"/>
        </label>

        <label htmlFor="password">
          Password
          <input type="password" name="password"/>
        </label>

        <label htmlFor="file">
          File
          <input type="file" name="file"/>
        </label>

        <label htmlFor="textarea">
          Text Area
          <textarea name="textarea"/>
        </label>

        <label htmlFor="select">
          Select
          <select name="select" id="select">
            <option value="option1">Option 1</option>
            <option value="option2">Option 2</option>
            <option value="option3">Option 3</option>
          </select>
        </label>

        <div>
          <input type="submit" value="Submit" />
          <input type="reset" value="Reset" />
        </div>
      </fieldset>
    </Form>
  </div>
);

