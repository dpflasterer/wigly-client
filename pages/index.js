import React from 'react';
import ItemList from '../components/ItemsList';

export default class Home extends React.Component {
  render() {
    return (
      <div>
        <h1>Wigly</h1>
        <p className="p">
          {this.props.userAgent}
        </p>
        <ItemList />
      </div>
    )
  }

  static async getInitialProps({ req }) {
    const userAgent = req ? req.headers['user-agent'] : navigator.userAgent;
    return { userAgent }
  }
}

