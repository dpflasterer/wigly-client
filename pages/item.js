import React from 'react';
import Item from '../components/Item';

export default class ItemPage extends React.Component {
  render() {
    return (
      <div>
        <h1>Wigly</h1>
        <Item query={this.props.query} />
      </div>
    )
  }
}
