import React, {Component} from 'react';
import {Query} from 'react-apollo';
import gql from 'graphql-tag';

class Item extends Component {
  render() {
    return (
      <div>
        <Query query={ITEM_QUERY} variables={{id: this.props.query.id || ''}}>
          {({data, error, loading}) => {
            if(loading) return <p>Loading...</p>;
            if(error) return <p>Error: {error.message}</p>;
            return <It key={data.item.id} item={data.item}/>
          }}
        </Query>
      </div>
    );
  }
}

export default Item;
export {ITEM_QUERY}

const ITEM_QUERY = gql`
  query ITEM_QUERY($id: ID!){
    item(where: {id: $id}){
      id
      title
      description
    }
  }`;


class It extends Component {
  render() {
    const {title, description} = this.props.item;
    return (
      <div>
        <p>{title}</p>
        <p>{description}</p>
      </div>
    );
  }
}