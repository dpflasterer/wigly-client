import React, {Component} from 'react';
import NProgress from 'nprogress';
import Router from 'next/router';
import styled from 'styled-components';
import Nav from './Nav';

class Header extends Component {
  render() {
    return (
      <NavWrap>
        <Nav/>
      </NavWrap>
    );
  }
}

export default Header;

Router.onRouteChangeStart = () => {
  NProgress.start();
};
Router.onRouteChangeComplete = () => {
  NProgress.done();
};
Router.onRouteChangeError = () => {
  NProgress.done();
};

const NavWrap = styled.div`
  border-bottom-style: solid;
  border-bottom-color: ${p => p.theme.colors.border};
  border-bottom-width: ${p => p.theme.borderWidth};
  
  padding: 0 1.5rem;
  @media (min-width: ${props => props.theme.breakpoints.mobile}){
    padding: 0 2rem;
  }
`;