import { Query, Mutation } from 'react-apollo';
import Error from  '../ErrorMessage';
import gql from 'graphql-tag';
import PropTypes from 'prop-types';

const possiblePermissions = [
  'ADMIN',
  'USER',
  'ITEM_CREATE',
  'ITEM_UPDATE',
  'ITEM_DELETE',
  'PERMISSION_UPDATE',
];

const Permissions = () => (
  <Query query={ALL_USERS_QUERY}>
    {({data, loading, error}) => (
      <div>
        <Error error={error}/>
        <p>Permissions</p>
        <table>
          <thead>
          <tr>
            <th>Name</th>
            <th>Email</th>
            {possiblePermissions.map(perm => <th key={perm}>{perm}</th>)}
            <th></th>
          </tr>
          </thead>
          <tbody>
          {data.users.map(user => <UserPermissions key={user.id} user={user}/>)}
          </tbody>
        </table>
      </div>
    )}
  </Query>
);

export default Permissions;

class UserPermissions extends React.Component{
  static propTypes = {
    user: PropTypes.shape({
      name: PropTypes.string,
      email: PropTypes.string,
      id: PropTypes.string,
      permissions: PropTypes.array,
    }).isRequired,
  };
  state = {
    permissions: this.props.user.permissions,
  };
  render(){
    const user = this.props.user;
    return (
      <Mutation mutation={UPDATE_PERMISSIONS_MUTATION}
                variables={{
                  permissions: this.state.permissions,
                  userId: this.props.user.id,
                }}
                refetchQueries={[{query: ALL_USERS_QUERY}]}>
        {(updatePermissions, {loading, error}) => (
        <>
          {error && <tr><td colspan="8"><Error error={error}/></td></tr>}
          <tr>
            <td>{user.name}</td>
            <td>{user.email}</td>
            {possiblePermissions.map(permission => (
              <td key={permission}>
                <label htmlFor={`${user.id}-permission-${permission}`}>
                  <input type="checkbox" id={`${user.id}-permission-${permission}`}
                         checked={this.state.permissions.includes(permission)}
                         value={permission}
                         onChange={e => this.handlePermissionChange(e, updatePermissions)} />
                </label>
              </td>
            ))}
            <td>
              <button type="button"
                      disabled={loading}
                      onClick={updatePermissions}>
                Updat{loading ? 'ing' : 'e'}
              </button>
            </td>
          </tr>
        </>
        )}
      </Mutation>
    )
  }

  handlePermissionChange = ({target: {value, checked}}, updatePermissions) => {
    let permissions = [...this.state.permissions];
    if(checked)
      permissions.push(value);
    else
      permissions = permissions.filter(permission => permission !== value);
    this.setState({permissions}, updatePermissions)
  }
}

const ALL_USERS_QUERY = gql`
  query ALL_USERS_QUERY {
    users{
      id
      name
      email
      permissions
    }
  }
`;

const UPDATE_PERMISSIONS_MUTATION = gql`
  mutation UPDATE_PERMISSIONS_MUTATION($permissions: [Permission], $userId: ID!) {
    updatePermissions(permissions: $permissions, userId: $userId){
      id
      name
      email
      permissions
    }
  }
`;