import React, {Component} from 'react';
import {Mutation, Query} from 'react-apollo';
import gql from 'graphql-tag';
import PropTypes from 'prop-types';
import Error from '../ErrorMessage';
import Form from '../common/Form';
import User from "./User";
import {REQUEST_RESET_MUTATION} from "./RequestReset";

class UserProfile extends Component {
  state = {};
  static propTypes = {
    user: PropTypes.shape({
      name: PropTypes.string,
      email: PropTypes.string,
      id: PropTypes.string,
      permissions: PropTypes.array,
    }).isRequired,
  };

  render() {
    const {user} = this.props;
    return (
      <>
      <div>
        <Mutation mutation={UPDATE_USER_MUTATION}
                  variables={{
                    ...this.state,
                    userId: user.id,
                  }}>
          {(update, {error, loading}) => {
            return (
              <Form method="post" onSubmit={async e => {
                e.preventDefault();
                await update();
              }}>
                <fieldset disabled={loading} aria-busy={loading}>
                  <h4>Profile</h4>
                  <Error error={error} />
                  <label htmlFor="email">
                    Email
                    <input type="email"
                           name="email" id="email"
                           placeholder="email"
                           defaultValue={user.email}
                           onChange={this.saveToState} />
                  </label>
                  <label htmlFor="name">
                    Name
                    <input type="text"
                           name="name"  id="name"
                           placeholder="name"
                           defaultValue={user.name}
                           onChange={this.saveToState} />
                  </label>
                  <div>
                    <input type="submit" value="Update"/>
                  </div>
                </fieldset>
              </Form>
            )
          }}
        </Mutation>

      </div>
      <div>
        <Mutation mutation={REQUEST_RESET_MUTATION}
                  variables={{
                    email: this.state.email || user.email
                  }}>
          {(reset, {error, loading, called}) => {
            return (
              <Form method="post" onSubmit={async e => {
                e.preventDefault();
                await reset();
              }}>
                <fieldset disabled={loading} aria-busy={loading}>
                  <h4>Request a Password Reset</h4>
                  <Error error={error} />
                  {!error && !loading && called && <p>Success! Check your email!</p>}
                  <div>
                    <input type="submit" value="Request Reset Password"/>
                  </div>
                </fieldset>
              </Form>
            )
          }}
        </Mutation>
      </div>
      </>
    );
  }

  saveToState = e => this.setState({[e.target.name]: e.target.value});
}
export default UserProfile;
export {Me, AdminUser};

class Me extends Component {
  render() {
    return (
      <User>
        {({data: {me}}, error, loading) => console.log({me}) || (
          <UserProfile user={me} />
        )}
      </User>
    );
  }
}

class AdminUser extends Component {
  render() {
    const {id, email} = this.props.query;
    return (
      <Query query={SINGLE_USER_QUERY}
             variables={{userId: id, email}}>
        {({data, error, loading}) => {
          if(error) return <Error error={error}/>

          const {user} = data;
          if(!user) return <p>No user found.</p>
          return <UserProfile user={user} />
        }}
      </Query>
    );
  }
}

const SINGLE_USER_QUERY = gql`
  query SINGLE_USER_QUERY($email: String, $userId: ID){
    user(email: $email, userId: $userId){
      id
      email
      name
    }
  }
`;

const UPDATE_USER_MUTATION = gql`
  mutation UPDATE_USER_MUTATION($email: String, $name: String, $userId: ID!){
    updateUser(email: $email, name: $name, userId: $userId){
      id
      email
      name
    }
  }
`;
