import React, {Component} from 'react';
import {Mutation} from 'react-apollo';
import gql from 'graphql-tag';
import Error from '../ErrorMessage';
import Form from '../common/Form';

class RequestReset extends Component {
  state = {
    email: '',
  };

  render() {
    return (
      <div>
        <Mutation mutation={REQUEST_RESET_MUTATION}
                  variables={this.state}>
          {(reset, {error, loading, called}) => {
            return (
              <Form method="post" onSubmit={async e => {
                e.preventDefault();
                await reset();
                this.setState({email: ''});
              }}>
                <fieldset disabled={loading} aria-busy={loading}>
                  <h4>Request a Password Reset</h4>
                  <Error error={error} />
                  {!error && !loading && called && <p>Success! Check your email!</p>}
                  <label htmlFor="email">
                    Email
                    <input type="email" name="email" placeholder="email" value={this.state.email} onChange={this.saveToState} />
                  </label>
                  <div>
                    <input type="submit" value="Request"/>
                  </div>
                </fieldset>
              </Form>
            )
          }}
        </Mutation>
      </div>
    );
  }

  saveToState = e => this.setState({[e.target.name]: e.target.value});
}
export default RequestReset;
export {REQUEST_RESET_MUTATION};


const REQUEST_RESET_MUTATION = gql`
  mutation REQUEST_RESET_MUTATION($email: String!){
    requestReset(email: $email){
      message
    }
  }
`;