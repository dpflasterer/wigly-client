import styled from 'styled-components';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import {CURRENT_USER_QUERY} from "./User";


const Signout = props => (
  <Mutation mutation={SIGN_OUT_MUTATION}
            refetchQueries={[{query: CURRENT_USER_QUERY}]}>
    {(signout) => <SignoutButton onClick={signout}>Sign Out</SignoutButton>}
  </Mutation>
);
export default Signout;


const SIGN_OUT_MUTATION = gql`
  mutation SIGN_OUT_MUTATION{
    signout{
      message
    }
  }
`;

export const SignoutButton = styled.button`
  display: flex;
  padding: .5rem;

  margin: 1rem 0;
  border-bottom: transparent solid .1rem;

  &:hover{
    border-bottom: ${props => props.theme.colors.linkHover} solid .1rem;
  }
`;