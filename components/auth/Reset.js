import React, {Component} from 'react';
import {Mutation} from 'react-apollo';
import gql from 'graphql-tag';
import PropTypes from 'prop-types';
import Error from '../ErrorMessage';
import Form from '../common/Form';
import {CURRENT_USER_QUERY} from "./User";

class Reset extends Component {
  static propTypes = {
    resetToken: PropTypes.string.isRequired
  };
  state = {
    password: '',
    confirmPassword: '',
  };

  render() {
    return (
      <div>
        <Mutation mutation={RESET_PASSWORD_MUTATION}
                  variables={{...this.state, resetToken: this.props.resetToken}}
                  refetchQueries={[{query: CURRENT_USER_QUERY}]}>
          {(reset, {error, loading, called}) => {
            return (
              <Form method="post" onSubmit={async e => {
                e.preventDefault();
                await reset();
                this.setState({email: '', password: '', passwordConfirm: ''});
              }}>
                <fieldset disabled={loading} aria-busy={loading}>
                  <h4>Reset Your Password</h4>
                  <Error error={error} />
                  <label htmlFor="password">
                    Password
                    <input type="password" name="password" placeholder="password" value={this.state.password} onChange={this.saveToState} />
                  </label>
                  <label htmlFor="confirmPassword">
                    Confirm Password
                    <input type="password" name="confirmPassword" placeholder="password" value={this.state.confirmPassword} onChange={this.saveToState} />
                  </label>
                  <div>
                    <input type="submit" value="Reset Your Password!"/>
                  </div>
                </fieldset>
              </Form>
            )
          }}
        </Mutation>
      </div>
    );
  }

  saveToState = e => this.setState({[e.target.name]: e.target.value});
}
export default Reset;


const RESET_PASSWORD_MUTATION = gql`
  mutation RESET_PASSWORD_MUTATION($resetToken: String!, $password: String!, $confirmPassword: String!){
    resetPassword(resetToken: $resetToken, password: $password, confirmPassword: $confirmPassword){
      id
      email
      name
    }
  }
`;