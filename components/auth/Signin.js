import React, {Component} from 'react';
import {Mutation} from 'react-apollo';
import gql from 'graphql-tag';
import Error from '../ErrorMessage';
import Form from '../common/Form';
import {CURRENT_USER_QUERY} from "./User";

class Signin extends Component {
  state = {
    email: '',
    password: '',
  };

  render() {
    return (
      <div>
        <Mutation mutation={SIGNIN_MUTATION}
                  variables={this.state}
                  refetchQueries={[{query: CURRENT_USER_QUERY}]}>
          {(signin, {error, loading}) => {
            return (
              <Form method="post" onSubmit={async e => {
                e.preventDefault();
                await signin();
                this.setState({email: '', password: ''});
              }}>
                <fieldset disabled={loading} aria-busy={loading}>
                  <h4>Sign in</h4>
                  <Error error={error} />
                  <label htmlFor="email">
                    Email
                    <input type="email" name="email" placeholder="email" value={this.state.email} onChange={this.saveToState} />
                  </label>
                  <label htmlFor="password">
                    Password
                    <input type="password" name="password" placeholder="password" value={this.state.password} onChange={this.saveToState} />
                  </label>
                  <div>
                    <input type="submit" value="Sign In"/>
                  </div>
                </fieldset>
              </Form>
            )
          }}
        </Mutation>
      </div>
    );
  }

  saveToState = e => this.setState({[e.target.name]: e.target.value});
}
export default Signin;


const SIGNIN_MUTATION = gql`
  mutation SIGNIN_MUTATION($email: String!, $password: String!){
    signin(email: $email, password: $password){
      id
      email
      name
    }
  }
`;