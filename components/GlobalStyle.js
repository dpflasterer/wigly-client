import {createGlobalStyle} from 'styled-components';


export const theme = {
  colors: {
    text: '#0F0F0F',
    subtext: `#555555`,
    link: `#35358C`,
    linkHover: '#5252ff',


    focus: '#35358C',
    loading: '#5252ff',

    border: '#555555',

    alert: '#8e1219',

  },
  breakpoints: {
    mobile: '425px',
    tablet: '768px',
    desktop: '1400px',
  },
  fontFamily: {
    heading: 'Courier',
    body: 'Helvetica',
  },
  fontSize: {
    p: '1.8rem',
    h1: '4.8rem',
    h2: '3.6rem',
    h3: '2.8rem',
    h4: '2.2rem',
    h5: '1.8rem',
  },
  lineHeight: {
    p: '2rem',
    h1: '4.8rem',
    h2: '3.6rem',
    h3: '2.8rem',
    h4: '2.2rem',
    h5: '1.8rem',
  },
  borderRadius: '.5rem',
  borderWidth: '.2rem',
};

const GlobalStyle = createGlobalStyle`
  html{
    box-sizing: border-box;
    font-size: 10px;
  }
  
  *, *:before, *:after{
    box-sizing: inherit;
  }
  
  body{
    padding: 0;
    margin: 0;
    font-family: ${theme.fontFamily.body};
    font-size: ${theme.fontSize.p};
    line-height: ${theme.lineHeight.p};
  }
  
  a,
  button{
    text-decoration: none;
    color: ${theme.colors.link};
    font-family: ${theme.fontFamily.body};
    font-size: ${theme.fontSize.p};
    line-height: ${theme.lineHeight.p};
    background: transparent;
    cursor: pointer;
    outline: 0;
    border: none;

    &:hover{
      color: ${theme.colors.linkHover};
    }
  }
  p{
    color: ${theme.colors.text};
    font-family: ${theme.fontFamily.body};
    font-size: ${theme.fontSize.p};
    line-height: ${theme.lineHeight.p};
    margin: 20px 0;
  }
  h1{
    color: ${theme.colors.text};
    font-family: ${theme.fontFamily.heading};
    font-size: ${theme.fontSize.h1};
    line-height: ${theme.lineHeight.h1};
    margin: 3rem 0;
  }
  h2{
    color: ${theme.colors.text};
    font-family: ${theme.fontFamily.heading};
    font-size: ${theme.fontSize.h2};
    line-height: ${theme.lineHeight.h2};
    margin: 2rem 0 1rem;
  }
  h3{
    color: ${theme.colors.subtext};
    font-family: ${theme.fontFamily.heading};
    font-size: ${theme.fontSize.h3};
    line-height: ${theme.lineHeight.h3};
    margin: 1.4rem 0 1rem;
  }
  h4{
    color: ${theme.colors.text};
    font-family: ${theme.fontFamily.heading};
    font-size: ${theme.fontSize.h4};
    line-height: ${theme.lineHeight.h4};
    margin: 1rem 0;
  }
  h5{
    color: ${theme.colors.subtext};
    font-family: ${theme.fontFamily.heading};
    font-size: ${theme.fontSize.h5};
    line-height: ${theme.lineHeight.h5};
    margin: 1rem 0;
  }
 
`;
export default GlobalStyle;