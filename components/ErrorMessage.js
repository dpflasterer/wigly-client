import styled from 'styled-components';
import React from 'react';
import PropTypes from 'prop-types';

const DisplayError = ({ error }) => {
  if (!error || !error.message) return null;
  if (error.networkError && error.networkError.result && error.networkError.result.errors.length) {
    return error.networkError.result.errors.map((error, i) => (
      <ErrorStyles key={i}>
        <p data-test="graphql-error">
          <strong>Shoot!</strong>
          {error.message.replace('GraphQL error: ', '')}
        </p>
      </ErrorStyles>
    ));
  }
  return (
    <ErrorStyles>
      <p data-test="graphql-error">
        <strong>Shoot!</strong>
        {error.message.replace('GraphQL error: ', '')}
      </p>
    </ErrorStyles>
  );
};

DisplayError.defaultProps = {
  error: {},
};

DisplayError.propTypes = {
  error: PropTypes.object,
};

export default DisplayError;


const ErrorStyles = styled.div`
  padding: 1rem;
  background: white;
  margin: 2rem 0;
  //border: 1px solid ${p => p.theme.colors.alert};
  border-style: solid;
  border-color: ${p => p.theme.colors.alert};
  border-width: ${p => p.theme.borderWidth};
  border-radius: ${p => p.theme.borderRadius};
  //border-left: 5px solid red;
  
  &:first-child{
    margin-top: 0;
  }
  &:last-child{
    margin-bottom: 0;
  }
  
  p {
    margin: 0;
    font-weight: 100;
  }
  strong {
    margin-right: 1rem;
  }
`;