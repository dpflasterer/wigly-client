import React from 'react';
import styled from 'styled-components';
import Link from 'next/link'
import Search from './Search';
import User from './auth/User';
import Signout from './auth/Signout';

const Nav = props => {
  return (
    <User>
      {({data: {me}}) => (
        <NavRow>
          <Link href="/" prefetch>
            <NavLink>Wigly</NavLink>
          </Link>
          <Link href="/about" prefetch>
            <NavLink>About</NavLink>
          </Link>

          <Link href="/style-guide" prefetch>
            <NavLink>Style Guide</NavLink>
          </Link>

          {me && (
            <>
              <Link href="/create" prefetch>
                <NavLink>Create</NavLink>
              </Link>
              <Link href="/me" prefetch>
                <NavLink>Account</NavLink>
              </Link>
              <Signout />
            </>
          )}
          {!me && (
            <Link href="/signup" prefetch>
              <NavLink>Sign In</NavLink>
            </Link>
          )}
          <Search />
        </NavRow>
      )}
    </User>

  );
};

export default Nav;

const NavRow = styled.div`
  max-width: ${props => props.theme.breakpoints.tablet};
  margin: 0 auto;
  
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
  align-items: baseline;
`;

export const NavLink = styled.a`
  display: flex;
  padding: .5rem;

  margin: 1rem 0;
  border-bottom: transparent solid .1rem;

  &:hover{
    border-bottom: ${props => props.theme.colors.linkHover} solid .1rem;
  }
`;