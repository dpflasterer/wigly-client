import React, {Component} from 'react';
import styled from 'styled-components';
import Downshift, {resetIdCounter} from 'downshift';
import Router from 'next/router';
import {ApolloConsumer } from 'react-apollo';
import gql from 'graphql-tag';
import debounce from 'lodash.debounce';
import {LoadingInput} from "./common/Inputs";
import {theme} from "./GlobalStyle";

const routeToArticle = ({id}) => {
  Router.push({pathname: '/item', query: {id}})
};

class Search extends Component {
  state = {
    items: [],
    loading: false,
  };

  render(){
    resetIdCounter();
    return (
      <SearchWrap>
        <Downshift onChange={routeToArticle}
                   itemToString={item => item === null ? '' : item.title}>
          {({getInputProps, getItemProps, isOpen, inputValue, highlightedIndex}) => (
            <div>
              <ApolloConsumer>
                {(client) => (
                  <LoadingInput {...getInputProps({
                    type: "search",
                    placeholder: "Search...",
                    id: "search",
                    name: "search",
                    loading: this.state.loading,
                    className: this.state.loading ? 'loading' : '',
                    onChange: e => {
                      e.persist();
                      this.triggerChange(e, client);
                    }
                    })}
                  />
                )}
              </ApolloConsumer>
              <ResultList>
              {isOpen &&
                this.state.items.map((item, i) => (
                  <ResultItem highlighted={i === highlightedIndex}
                            key={item.id}>
                    <Item {...getItemProps({item})}
                          item={item}
                          highlighted={i === highlightedIndex}/>
                  </ResultItem>
                ))
              }
              { isOpen &&
                !this.state.items.length &&
                !this.state.loading &&
                inputValue && (
                <ResultItem>Nothing Found for "{inputValue}"</ResultItem>
              )}
              </ResultList>
            </div>
          )}
        </Downshift>
      </SearchWrap>
    );
  }

  triggerChange = async (e, client) => {
    // trigger loading then debounce request
    this.setState({loading: true});
    this.handleChange(e, client);
  };

  handleChange = debounce(async (e, client) => {
    // clear if empty search value
    if(e.target.value === '') return this.clear();

    // request and update
    const res = await client.query({
      query: AUTOCOMPLETE_ITEMS,
      variables: {search: e.target.value}
    });
    this.setState({
      items: res.data.items,
      loading: false
    })
  }, 'delay' in this.props ? this.props.delay : 250);

  clear = () => {
    this.setState({
      items: [],
      loading: false
    })
  }
}

export default Search;
export {AUTOCOMPLETE_ITEMS};

const AUTOCOMPLETE_ITEMS = gql`
  query AUTOCOMPLETE_ITEMS(
    $search: String!
  ){
    items(where: {
      OR: [{
        title_contains: $search
      },{
          description_contains: $search
      }]
    }){
      id
      title
    }
  }`;

export const SearchWrap = styled.div`
  display: flex;
  position: relative;
  
  input{
    width: 100%;
    padding: 1rem;
    margin: 1rem 0;

    border-style: solid;
    color: ${p => p.theme.colors.text};
    background: transparent;
    border-color: ${p => p.theme.colors.border};
    border-width: ${p => p.theme.borderWidth};
    border-radius: ${p => p.theme.borderRadius};
    font-family: ${p => p.theme.fontFamily.body};
    font-size: ${p => p.theme.fontSize.p};
    line-height: ${p => p.theme.lineHeight.p};
    &:focus {
      outline: 0;
      border-color: ${p => p.theme.colors.focus};
    }
  }
`;
SearchWrap.defaultProps = {theme};

export const ResultList = styled.div`
  position: absolute;
  width: 100%;
`;

export const ResultItem = styled.div`
  background: ${props => (props.highlighted ? props.theme.colors.focus : 'none')};
  width: 100%;
  margin: 5px 0;
  padding: 10px;
  p{
    margin: 0;
  }
  
  
`;
ResultItem.defaultProps = {
  highlighted: false,
  theme: {
    colors: {
      focus: '#444444',
    }
  }
};

class Item extends Component {
  render() {
    const {title} = this.props.item;
    return (
      <div>
        <p>{title}</p>
      </div>
    );
  }
}