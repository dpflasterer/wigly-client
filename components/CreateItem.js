import React, {Component} from 'react';
import {Mutation} from 'react-apollo';
import gql from 'graphql-tag';
import Router from 'next/router';
import Error from './ErrorMessage';
import Form from './common/Form';

class CreateItem extends Component {
  state = {
    title: '',
    description: '',
    image: '',
    largeImage: '',
  };
  render() {
    return (
      <Mutation mutation={CREATE_ITEM_MUTATION} variables={this.state}>
        {(createItem, {loading, error}) => (
          <Form onSubmit={async e => {
            // stop default submit
            e.preventDefault();
            // call the mutation
            const res = await createItem();
            // route to new page
            Router.push({
              pathname: '/item',
              query: { id: res.data.createItem.id}
            })
          }}>
            <Error error={error}></Error>
            <fieldset disabled={loading} aria-busy={loading}>
              <label htmlFor="title">
                Title
                <input type="text" id="title" name="title" placeholder="Title" value={this.state.title}  onChange={this.handleChange} required/>
              </label>
              <label htmlFor="description">
                Description
                <textarea id="description" name="description" placeholder="Description" value={this.state.description}  onChange={this.handleChange} required/>
              </label>
              <div>
                <button type="submit">Submit</button>
              </div>
            </fieldset>
          </Form>
        )}
      </Mutation>
    );
  }

  handleChange = e => {
    const {name, type, value} = e.target;
    const val = type === 'number' ? parseFloat(value) : value;
    this.setState({[name]: val})
  }
}

CreateItem.propTypes = {};

export default CreateItem;
export {CREATE_ITEM_MUTATION};


const CREATE_ITEM_MUTATION = gql`
  mutation CREATE_ITEM_MUTATION (
  $title: String!
  $description: String!
  ){
    createItem(
      title: $title
      description: $description
    ){
      title
      description
      id
    }
  }
`;