import React, {Component} from 'react';
import {Query} from 'react-apollo';
import gql from 'graphql-tag';
import Link from 'next/link';

class ItemsList extends Component {
  render() {
    return (
      <div>
        <Query query={ALL_ITEMS}>
          {({data, error, loading}) => {
            if(loading) return <p>Loading...</p>;
            if(error) return <p>Error: {error.message}</p>;
            return data.items.map(item => <Item key={item.id} item={item}/>);
          }}
        </Query>
      </div>
    );
  }
}

export default ItemsList;
export {ALL_ITEMS}

const ALL_ITEMS = gql`
  query ALL_ITEMS{
    items{
      id
      title
      description
    }
  }`;



class Item extends Component {
  render() {
    const {id, title, description} = this.props.item;
    return (
      <div>
        <Link href={{pathname: "/item", query: {id}}} >
          <a>{title}</a>
        </Link>
        <p>{description}</p>
      </div>
    );
  }
}