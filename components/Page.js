import React, {Component} from 'react';
import styled, {ThemeProvider} from 'styled-components';
import Header from './Header'
import Meta from './Meta';
import GlobalStyle, {theme} from './GlobalStyle';


class MyComponent extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <PageContainer>
          <Meta/>
          <GlobalStyle />
          <Header />
          <Inner>
            {this.props.children}
          </Inner>
        </PageContainer>
      </ThemeProvider>
    );
  }
}

export default MyComponent;


const PageContainer = styled.div`
  position:relative;
`;
const Inner = styled.div`
  max-width: ${props => props.theme.breakpoints.tablet};
  margin: 0 auto;
  
  padding: 0 1.5rem;
  @media (min-width: ${props => props.theme.breakpoints.mobile}){
    padding: 0 2rem;
  }
`;
