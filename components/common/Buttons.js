import styled from 'styled-components'

export const Button = styled.button`
  color: ${props => props.theme.colors.link};
  background: transparent;
  font-size: ${props => props.theme.fontSize.p};
  border-color: ${props => props.theme.colors.border};
  border-width: ${props => props.theme.borderWidth};
  border-radius: ${props => props.theme.borderRadius};
  border-style: solid;
  padding: 1rem;
  margin: 1rem;
  &:hover{
    color: ${props => props.theme.colors.linkHover};
    border-color: ${props => props.theme.colors.linkHover};
  }
`;
Button.defaultProps = {
  theme: {
    fontSize: {
      p: '1.8rem',
    },
    colors: {
      link: '#35358C',
      linkHover: '#35358C',
    }
  }
};
