import styled from 'styled-components'

export const LoadingInput = styled.input`
  //border-color: ${props => (props.loading ? props.theme.colors.loading : 'inherit')};
  &:focus{
    border-color: ${props => (props.loading ? props.theme.colors.loading : props.theme.colors.focus)};
  }
`;
LoadingInput.defaultProps = {
  theme: {
    colors: {
      loading: 'lightblue'
    }
  }
};