import styled, { keyframes } from 'styled-components';

const loading = keyframes`
  from {
    background-position: 0 0;
    /*rotate: 0;*/ 
  }

  to {
    background-position: 100% 100%;
    /*rotate: 360deg;*/ 
  }
`;

const Form = styled.form`
  padding: 3rem;
  margin: 1rem 1rem 2rem;
  border-style: solid;
  border-color: ${p => p.theme.colors.border};
  border-width: ${p => p.theme.borderWidth};
  border-radius: ${p => p.theme.borderRadius};
  label {
    display: block;
    margin-bottom: 1rem;
    font-weight: bold;
  }
 
  input,
  textarea,
  select {
    width: 100%;
    padding: 1rem;
    margin: 1rem 0;

    border-style: solid;
    color: ${p => p.theme.colors.text};
    background: transparent;
    border-color: ${p => p.theme.colors.border};
    border-width: ${p => p.theme.borderWidth};
    border-radius: ${p => p.theme.borderRadius};
    font-family: ${p => p.theme.fontFamily.body};
    font-size: ${p => p.theme.fontSize.p};
    line-height: ${p => p.theme.lineHeight.p};
    &:focus {
      outline: 0;
      border-color: ${p => p.theme.colors.focus};
    }
  }
  button,
  input[type='submit'],
  input[type='reset'] {
    width: auto;
    color: ${props => props.theme.colors.link};
    background: transparent;
    font-size: ${props => props.theme.fontSize.p};
    border-color: ${props => props.theme.colors.border};
    border-width: ${props => props.theme.borderWidth};
    border-radius: ${props => props.theme.borderRadius};
    border-style: solid;
    padding: 1rem;
    margin: 0 1rem;
   
    &:hover{
      color: ${props => props.theme.colors.linkHover};
      border-color: ${props => props.theme.colors.linkHover};
    }
    
    &:first-child{
      margin-left: 0;
    }
    &:last-child{
      margin-right: 0;
    }
  }
  fieldset {
    border: 0;
    padding: 0;

    &[disabled] {
      opacity: 0.5;
    }
    &[aria-busy='true']::before {
      background-size: 50% auto;
      animation: ${loading} 0.5s linear infinite;
    }
  }
`;

Form.displayName = 'Form';

export default Form;
